<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Spring 3 MVC Multipe Row Submit</title>
</head>
<body>
<h2>Show Contacts</h2>
<table width="50%">
	<tr>
        <td colspan="4">User Name: ${contactForm.username}</td>
    </tr>
	<tr>
		<th>Name</th>
		<th>Last name</th>
		<th>Email</th>
		<th>Phone</th>
		<th>Gender</th>
	</tr>
	<c:forEach items="${contactForm.contacts}" var="contact" varStatus="status">
		<tr>
			<td>${contact.firstname}</td>
			<td>${contact.lastname}</td>
			<td>${contact.email}</td>
			<td>${contact.phone}</td>
			<td>${contact.gender}</td>
		</tr>
	</c:forEach>
</table>	
<br/>
<input type="button" value="Back" onclick="javascript:history.back()"/>
</body>
</html>