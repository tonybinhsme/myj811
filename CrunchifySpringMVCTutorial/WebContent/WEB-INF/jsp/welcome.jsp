<%@ page language="java" contentType = "text/html; charset = UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; UTF-8">
	<title>Welcome</title>
	<style type="text/css">
		body {
			background-image: url('https://cdn.crunchify.com/bg.png');
		}
	</style>
</head>
<body>
	<h3>${message}</h3>
	
	<table>
		<tr>
			<td>Welcome ${firstname}</td>
		</tr>
		<tr>
			<td><a href="home.jsp">Home</a></td>
		</tr>
	</table>
</body>
</html>