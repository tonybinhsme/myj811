<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring 3 MVC Multipe Row Submit</title>
<script src ="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<script language="javascript">
function addRow(tableID) {
	var lastIndex = document.getElementById("lastIndex").value;
    var table = document.getElementById(tableID);

    ////var rowCount = table.rows.length - 2;
	var rowCount = table.rows.length;
    console.log("rowCount: " + rowCount);

    //var rowTR = $("#"+tableID+" tr").length;
    //alert(rowTR); 
    
    if (rowCount>4){
    	alert("Stop");
    	return;
    }
    
    var row = table.insertRow(rowCount);
    
    var cell0 = row.insertCell(0);
	cell0.innerHTML = rowCount;
	
	//var length=(table.rows.length)-1;
	///var length = rowCount - 1;
	var length = lastIndex;
	
	//firstname
	var cell1 = row.insertCell(1);	
    var element1 = document.createElement("input");
    element1.type = "text";
    element1.name="contacts["+length+"].firstname";
    cell1.appendChild(element1);
    
    //lastname
    var cell2 = row.insertCell(2);
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.name="contacts["+length+"].lastname";
    cell2.appendChild(element2);
    
    //email
    var cell3 = row.insertCell(3);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.name="contacts["+length+"].email";
    cell3.appendChild(element3);
    
	//phone
    var cell4 = row.insertCell(4);
    var element4 = document.createElement("input");
    element4.type = "text";
    element4.name="contacts["+length+"].phone";
    cell4.appendChild(element4);
    
    //gender
    var cell5 = row.insertCell(5);
    //Male
    var element5 = document.createElement("input");
    element5.type = "radio";
    element5.value = "Male";
    element5.name="contacts["+length+"].gender";
    var element6 = document.createElement("label");
    element6.innerHTML = "Male";
    
    //Female
    var element7 = document.createElement("input");
    element7.type = "radio";
    element7.value = "Female";
    element7.name="contacts["+length+"].gender";
    var element8 = document.createElement("label");
    element8.innerHTML = "Female";
    
    cell5.appendChild(element5);
    cell5.appendChild(element6);
    cell5.appendChild(element7);
    cell5.appendChild(element8);
    
    //tao button delete
    var cell6 = row.insertCell(6);
    var element9 = document.createElement("input");
    element9.type = "button";
    element9.value = "Delete me " + rowCount;
    element9.setAttribute("style","color:red;font-size:14px");
    element9.onclick = function() {
    	removeRow(this);
	};
    cell6.appendChild(element9);
    
    lastIndex++;
    document.getElementById("lastIndex").value = lastIndex;
}

function deleteRow(tableID) {
    try {
		var table = document.getElementById(tableID);
	    var rowCount = table.rows.length - 3;
	    console.log(rowCount);
	    if (rowCount>0){
	    	table.deleteRow(rowCount);
	    }
    }catch(e) {
    	alert(e);
    }
}

function removeRow(el) {
	if(!confirm("Are you sure you want to delete?")) return;
	
	var tbl = el.parentNode.parentNode.parentNode;
    var row = el.parentNode.parentNode.rowIndex;
    tbl.deleteRow(row);
    
    var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	if (rowCount==1){
		document.getElementById("lastIndex").value = 0;
	}
}
</script>	
</head>
<body>
<h2>Spring MVC Multiple Row Form Submit example</h2>
<form:form method="post" action="save" modelAttribute="contactForm">
	<table>
	    <tr>
           	<td><form:label path = "favoriteFrameworks">Favorite Web Frameworks</form:label></td>
			<td><form:checkboxes items = "${webFrameworkList}" path = "favoriteFrameworks" /></td>
			<td></td>       
        </tr>
        <tr>
			<td><form:label path = "favoriteNumber">Favorite Number</form:label></td>
            <td><form:radiobuttons path = "favoriteNumber" items = "${numbersList}" /></td>
            <td></td>
		</tr>
        <tr>
        	<td><form:label path = "country">Country</form:label></td>
            <td>
            	<form:select path = "country">
                	<form:option value = "NONE" label = "Select"/>
                    <form:options items = "${countryList}" />
				</form:select>     	
			</td>
			<td></td>
		</tr>  
        <tr>
        	<td><form:label path = "skills">Skills</form:label></td>
            <td><form:select path = "skills" items = "${skillsList}" multiple = "true" /></td>
            <td></td>
		</tr>
		<tr>
        	<td><form:label path = "username">User Name</form:label></td>
    		<td><form:input path = "username" value="Tony Binh" /></td>
    		<td><input type="button" value="Add Row" onclick="addRow('dataTable')" /></td>
	    </tr>
	    <tr>
	    	<td colspan="3"><input type="text" name="lastIndex" id="lastIndex" value="0" /></td>
	    </tr>
	</table><br/>
	
	<table id="dataTable">
		<thead>
			<tr>
				<th>No.</th>
				<th>Name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Gender</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${contactForm.contacts}" var="contact" varStatus="status">
				<tr>
					<td align="center">${status.count}</td>
					<td><input name="contacts[${status.index}].firstname" value="${contact.firstname}"/></td>
					<td><input name="contacts[${status.index}].lastname" value="${contact.lastname}"/></td>
					<td><input name="contacts[${status.index}].email" value="${contact.email}"/></td>
					<td><input name="contacts[${status.index}].phone" value="${contact.phone}"/></td>
					<td>
						<input type="radio" name="contacts[${status.index}].gender" value="Male" ${(contact.gender == 'Male' ? 'checked="checked"' : '')} />
						<label for="male">Male</label>
						<input type="radio" name="contacts[${status.index}].gender" value="Female" ${(contact.gender == 'Female' ? 'checked="checked"' : '')} />
  						<label for="female">Female</label>
					</td>
				</tr>
			</c:forEach>
		</tbody>
		<!--tfoot>
			<tr>
				<td colspan="6"></td>
				<td align="right">
					<input type="button" value="Add Row" onclick="addRow('dataTable')" />	 
					<input type="button" value="Delete Row" onclick="deleteRow('dataTable')" />
				</td>
			<tr>
		</tfoot-->
		
	</table>
	<br/><br/>
	<input type="submit" value="Save" />
</form:form>
</body>
</html>