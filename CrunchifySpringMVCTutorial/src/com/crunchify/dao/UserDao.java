package com.crunchify.dao;

import com.crunchify.model.Login;
import com.crunchify.model.User;

public interface UserDao {
	int register(User user);
	User validateUser(Login login);
}
