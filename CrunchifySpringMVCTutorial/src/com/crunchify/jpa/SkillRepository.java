package com.crunchify.jpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.crunchify.model.Skill;

public interface SkillRepository extends CrudRepository<Skill, Long> {
	public List<Skill> findByLabel(String label);
}