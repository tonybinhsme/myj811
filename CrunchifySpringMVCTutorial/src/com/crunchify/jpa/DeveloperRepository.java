package com.crunchify.jpa;

import org.springframework.data.repository.CrudRepository;

import com.crunchify.model.Developer;

public interface DeveloperRepository extends CrudRepository<Developer, Long> {

}
