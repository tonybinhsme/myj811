package com.crunchify.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.crunchify.dao.UserDao;
import com.crunchify.model.Login;
import com.crunchify.model.User;

public class UserServiceImpl implements UserService {
	@Autowired
	public UserDao userDao;
	
	public int register(User user) {
		return userDao.register(user);
	}
	
	public User validateUser(Login login) {
		return userDao.validateUser(login);
	}
}
