package com.crunchify.service;

import com.crunchify.model.Login;
import com.crunchify.model.User;

public interface UserService {
	int register(User user);
	User validateUser(Login login);
}
