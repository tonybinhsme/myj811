package com.crunchify.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.crunchify.model.Contact;
import com.crunchify.model.ContactForm;

@Controller
public class ContactController {
	private static List<Contact> contacts = new ArrayList<Contact>();

	static {
		contacts.add(new Contact("Barack", "Obama", "barack.o@whitehouse.com", "147-852-965", "Male"));
		contacts.add(new Contact("George", "Bush", "george.b@whitehouse.com", "785-985-652", "Female"));
		contacts.add(new Contact("Bill", "Clinton", "bill.c@whitehouse.com", "236-587-412", "Female"));
		contacts.add(new Contact("Ronald", "Reagan", "ronald.r@whitehouse.com", "369-852-452", "Male"));
	}
	
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public ModelAndView get() {
		
		ContactForm contactForm = new ContactForm();
		contactForm.setFavoriteFrameworks((new String []{"Spring MVC","Struts 2"}));
		//contactForm.setContacts(contacts);
		
		return new ModelAndView("add_contact" , "contactForm", contactForm);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("contactForm") ContactForm contactForm) {
		System.out.println(">>>ContactForm: " + contactForm);
		
		System.out.println(">>> userName: " + contactForm.getUsername());
		System.out.println(">>> getContacts: " + contactForm.getContacts().toString());

		List<Contact> contacts = contactForm.getContacts();
		
		if(null != contacts && contacts.size() > 0) {
			ContactController.contacts = contacts;
			for (Contact contact : contacts) {
				System.out.printf(">>> %s \t %s \t %s \n", contact.getFirstname(), contact.getLastname(), contact.getGender());
			}
		}
		
		return new ModelAndView("show_contact", "contactForm", contactForm);
	}
	
	@ModelAttribute("webFrameworkList")
	public List<String> getWebFrameworkList() {
		List<String> webFrameworkList = new ArrayList<String>();
	    webFrameworkList.add("Spring MVC");
	    webFrameworkList.add("Struts 1");
	    webFrameworkList.add("Struts 2");
	    webFrameworkList.add("Apache Wicket");
	    return webFrameworkList;
	}
	
	@ModelAttribute("numbersList")
	public List<String> getNumbersList() {
		List<String> numbersList = new ArrayList<String>();
	    numbersList.add("1");
	    numbersList.add("2");
	    numbersList.add("3");
	    numbersList.add("4");
	    return numbersList;
	}

	@ModelAttribute("countryList")
	public Map<String, String> getCountryList() {
		Map<String, String> countryList = new HashMap<String, String>();
	    countryList.put("US", "United States");
	    countryList.put("CH", "China");
	    countryList.put("SG", "Singapore");
	    countryList.put("MY", "Malaysia");
	    return countryList;
	}

	@ModelAttribute("skillsList")
	public Map<String, String> getSkillsList() {
		Map<String, String> skillList = new HashMap<String, String>();
	    skillList.put("Hibernate", "Hibernate");
	    skillList.put("Spring", "Spring");
	    skillList.put("Apache Wicket", "Apache Wicket");
	    skillList.put("Struts", "Struts");
	    return skillList;
	}
}
