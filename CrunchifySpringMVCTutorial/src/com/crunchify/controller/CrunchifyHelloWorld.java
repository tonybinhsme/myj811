package com.crunchify.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CrunchifyHelloWorld {

	 @RequestMapping("/welcome")
	 public ModelAndView helloWorld() {
		 String message = "binhtt14";
		 System.out.println(message+"");
		 return new ModelAndView("welcome", "message", message);
	 }
	
	 
	 @RequestMapping(value = "/hello", method = RequestMethod.GET)
	 public String printHello(ModelMap model) {
		 model.addAttribute("message", "Hello Spring MVC Framework!");
	     return "hello";
	 }
	
}
